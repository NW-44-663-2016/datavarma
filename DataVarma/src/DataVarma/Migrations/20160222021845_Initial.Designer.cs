using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataVarma.Models;

namespace DataVarma.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222021845_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataVarma.Models.Car", b =>
                {
                    b.Property<int>("CarId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color")
                        .IsRequired();

                    b.Property<string>("EngineType");

                    b.Property<string>("GearType");

                    b.Property<int?>("LocationID");

                    b.Property<string>("Model")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Wheelcount");

                    b.HasKey("CarId");
                });

            modelBuilder.Entity("DataVarma.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<int?>("CarCarId");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataVarma.Models.Location", b =>
                {
                    b.HasOne("DataVarma.Models.Car")
                        .WithMany()
                        .HasForeignKey("CarCarId");
                });
        }
    }
}
